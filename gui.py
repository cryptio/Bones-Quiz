from tkinter import *


class GUI(object):
    """A generalized GUI class that is meant to be inherited and derived from"""

    def __init__(self, root):
        self.canvas = Canvas(root)

        self.frame = Frame(self.canvas)
        self.frame.grid(row=0, column=0, sticky=NSEW)

        self.vertical_scrollbar = Scrollbar(root, orient=VERTICAL, command=self.canvas.yview)
        self.vertical_scrollbar.pack(side=RIGHT, fill=Y)

        self.horizontal_scrollbar = Scrollbar(root, orient=HORIZONTAL, command=self.canvas.xview)
        self.horizontal_scrollbar.pack(side=BOTTOM, fill=X)

        self.canvas.configure(yscrollcommand=self.vertical_scrollbar.set)
        self.canvas.configure(xscrollcommand=self.horizontal_scrollbar.set)

        self.canvas.pack(side=LEFT, fill=BOTH, expand=True)
        self.canvas.create_window((0, 0), window=self.frame)

        self.frame.bind("<Configure>", self.frame_configure_callback)

    def frame_configure_callback(self, event):
        """Reset the scroll region to encompass the inner frame"""

        self.canvas.configure(scrollregion=self.canvas.bbox(ALL))
