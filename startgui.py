from gui import GUI
from tkinter import *


class StartGUI(GUI):
    """A pregame GUI"""

    def __init__(self, root):
        super(StartGUI, self).__init__(root)

        root.geometry('650x320')
        max_grid_size = 10
        for grid_position in range(0, max_grid_size):
            root.grid_rowconfigure(grid_position, weight=1)
            root.grid_columnconfigure(grid_position, weight=1)
            self.frame.grid_rowconfigure(grid_position, weight=1)
            self.frame.grid_columnconfigure(grid_position, weight=1)
            self.canvas.grid_rowconfigure(grid_position, weight=1)
            self.canvas.grid_columnconfigure(grid_position, weight=1)

        self.img = PhotoImage(file='images/skull.png')
        self.img = self.img.zoom(25)
        self.img = self.img.subsample(75)
        self.img_label = Label(self.frame, image=self.img, anchor=CENTER)
        self.img_label.grid(row=0, column=0, sticky=NSEW)

        self.label = Label(self.frame, text='Welcome to the Bones Quiz!', anchor=CENTER)
        self.label.grid(row=1, column=0, sticky=NSEW)

        how_to_play = """
        How to play:
        For each screen, look at the provided image and description and try to guess the correct term.
        Type your answer in the text box, then press Submit.
        Click the button below to start.
        """

        how_to_play_label = Label(self.frame, text=how_to_play, anchor=CENTER)
        how_to_play_label.grid(row=3, column=0, sticky=NSEW)

        start_button = Button(self.frame, text='Start', command=root.destroy, anchor=CENTER)
        start_button.grid(row=4, column=0, sticky=NSEW)
