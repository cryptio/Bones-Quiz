from gui import GUI
from question import *
from tkinter import *


class ResultGUI(GUI):
    """Displays the results at the end of the quiz"""

    def __init__(self, root):
        self.correct = 0
        self.incorrect = 0

        super(ResultGUI, self).__init__(root)

        root.geometry('300x300')
        max_grid_size = 10
        for grid_position in range(0, max_grid_size):
            root.grid_rowconfigure(grid_position, weight=1)
            root.grid_columnconfigure(grid_position, weight=1)
            self.frame.grid_rowconfigure(grid_position, weight=1)
            self.frame.grid_columnconfigure(grid_position, weight=1)
            self.canvas.grid_rowconfigure(grid_position, weight=1)
            self.canvas.grid_columnconfigure(grid_position, weight=1)

    def display_results(self, results):
        """Display the results

            :param results: The results to display
            :type results: dict
        """

        label = Label(self.frame, text='Results', anchor=CENTER)
        label.grid(row=0, column=0, sticky=NSEW)

        question_results = ''
        for number, result in enumerate(results.keys()):
            question_results += str(number) + ': ' + result + ' : ' + results[result].name + '\n'
            if results[result] == QuestionResult.CORRECT:
                self.correct += 1
            elif results[result] == QuestionResult.INCORRECT:
                self.incorrect += 1
            else:
                print(f"Question '{result}' does not have a valid enum value")

        score_text = 'You got {} out of {} correct\n{}%'.format(self.correct, self.correct + self.incorrect, self.percentage())

        score_label = Label(self.frame, text=score_text, anchor=CENTER)
        score_label.grid(row=1, column=0, sticky=NSEW)

        question_results_label = Label(self.frame, text=question_results, anchor=CENTER)
        question_results_label.grid(row=2, column=0, sticky=NSEW)

    def percentage(self):
        """Return the score in a percentage

            :return The percentage with a two decimal point precision
            :rtype: int
        """

        score_sum = self.correct + self.incorrect
        assert score_sum  # stop divide by 0 errors

        percentage = self.correct / score_sum * 100

        return "{0:.2f}".format(percentage)
