from random import shuffle
from tkinter import *
from startgui import StartGUI
from quizgui import QuizGUI
from resultgui import ResultGUI
from question import *


def read_question_data(file):
    """Read data from the data file

        :param file: The file to read question data from
        :type file: string containing an existing file path
        :return data_list: list of Questions containing the data read from the file
        :rtype: list
    """

    data_list = []
    with open(file, 'r') as data_file:
        data_pat = re.compile(R'([A-Za-z ]+):([A-Za-z,.\' ]+):([A-Za-z/]+.\w+)')
        for line in data_file.readlines():
            matches = data_pat.match(line)

            if matches:
                name = matches.group(1)
                description = matches.group(2)
                image = matches.group(3)
                question = Question(name, description, image)
                data_list.append(question)
            else:
                print(f'{line} does not match, skipping...')

    return data_list


def output(output_level, msg):
    """Outputs messages to the terminal

        :param output_level: The output level of the message to log
        :param msg: The message to log
        :type output_level: OutputType
        :type msg: str
    """

    print('[' + str(output_level.name) + ']:' + msg)


def quiz_handle():
    """Handles the game

    .. todo::
            * Functionality for restarting the quiz without relaunching the script
    """

    gui_title = 'Bones Quiz'

    startroot = Tk()
    startroot.title(gui_title)
    startgui = StartGUI(startroot)
    startroot.mainloop()

    quizroot = Tk()
    quizroot.title(gui_title)
    quizgui = QuizGUI(quizroot)

    data = read_question_data('data')
    word_bank = []
    for question in data:
        word_bank.append(question.name)

    # shuffle the order of the data
    shuffle(data)
    shuffle(word_bank)

    results = {}

    for question in data:  # loop through each question
        quizgui.change_word_bank(', '.join([word for word in word_bank]))
        quizgui.change_image(question.image)
        quizgui.change_description(question.description)
        quizroot.update()

        while not quizgui.input:  # wait for input
            quizroot.update()

        # compare the input ignoring whitespace and capitalization
        quizgui_input_raw = quizgui.input.replace(' ', '').lower()
        question_name_raw = question.name.replace(' ', '').lower()
        if quizgui_input_raw == question_name_raw:
            print('Correct')
            word_bank.remove(question.name)  # remove the user's guess from the word bank
            results[question.name] = QuestionResult.CORRECT
        else:
            print('Incorrect')
            results[question.name] = QuestionResult.INCORRECT

        quizgui.input = None  # reset the input buffer
        quizgui.input_box.delete('0', END)  # clear the entry text

    quizroot.destroy()

    resultroot = Tk()
    resultroot.title(gui_title)
    resultgui = ResultGUI(resultroot)
    resultgui.display_results(results)
    resultroot.mainloop()


def main():
    try:
        quiz_handle()
    except TclError as e:  # Tkinter related errors
        print('TKinter error: ' + str(e))
        sys.exit(1)
    except KeyboardInterrupt:
        print('KeyboardInterrupt, exiting gracefully...')
    except Exception as e:
        print('Error: ' + str(e))


if __name__ == '__main__':
    main()
