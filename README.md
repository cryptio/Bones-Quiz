# Bones Quiz
This GUI quiz was written in April of 2018 for a grade 11 Human Biology project using Tkinter.

The quiz reads the data from the `data` file, with each line consisting of the answer (term), followed by the description, and lastly the corresponding image. 

## Requirements
* python3
* tkinter

## Usage
python3 bonesquiz.py

## Modifying questions
You may alter the quiz questions by modifying the `data` file. As described above, the syntax is as follows: `answer:description:image`.