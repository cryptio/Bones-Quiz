from gui import GUI
from tkinter import *


class QuizGUI(GUI):
    """GUI class for the quiz windows"""

    def __init__(self, root):
        super(QuizGUI, self).__init__(root)

        root.geometry('800x400')
        max_grid_size = 10
        for grid_position in range(0, max_grid_size):
            root.grid_rowconfigure(grid_position, weight=1)
            root.grid_columnconfigure(grid_position, weight=1)
            self.frame.grid_rowconfigure(grid_position, weight=1)
            self.frame.grid_columnconfigure(grid_position, weight=1)
            self.canvas.grid_rowconfigure(grid_position, weight=1)
            self.canvas.grid_columnconfigure(grid_position, weight=1)

        self.word_bank_label = Label(self.frame, text='Word Bank:', anchor=CENTER)
        self.word_bank_label.grid(row=0, column=3, sticky=NSEW)

        self.word_bank = Label(self.frame, text='test, test2, test3, test4, test5', anchor=CENTER)
        self.word_bank.grid(row=1, column=3, sticky=NSEW)

        self.img = PhotoImage(file='images/black.png')
        self.img = self.img.zoom(25)
        self.img = self.img.subsample(75)
        self.img_label = Label(self.frame, image=self.img, anchor=CENTER)
        self.img_label.grid(row=3, column=3, sticky=NSEW)

        self.description = Label(self.frame, text='test', anchor=CENTER)
        self.description.grid(row=4, column=3, sticky=NSEW)

        input_label = Label(self.frame, text='Your answer:', anchor=CENTER)
        input_label.grid(row=6, column=2, sticky=NSEW)

        self.input_box = Entry(self.frame)
        self.input_box.grid(row=6, column=3, sticky=NSEW)

        self.input_box.bind('<Return>', self.input_callback)  # bind the enter key

        submit_button = Button(self.frame, text='Submit', command=self.input_callback, anchor=CENTER)
        submit_button.grid(row=6, column=4, sticky=NSEW)

        self.input = None

    def input_callback(self, event=None):
        """Callback function for the Submit button"""

        self.input = self.input_box.get()

    def change_image(self, new_image):
        """Change the question's image

            :param new_image: The new image
            :type new_image: str
        """

        new_img = PhotoImage(file=new_image)
        self.img_label.configure(image=new_img, anchor=CENTER)
        self.img_label.image = new_img

    def change_description(self, new_description):
        """Change the question's description

            :param new_description: The text to put into the description
            :type new_description: str
        """

        self.description['text'] = 'Description: ' + new_description

    def change_word_bank(self, new_word_bank):
        """Change the current word_bank

            :param new_word_bank: The text to put into the word bank
            :type new_word_bank: str
        """

        # insert a newline character every 96 characters to avoid overflow
        max_line_length = 96
        word_bank_length = len(new_word_bank)
        new_word_bank = '\n'.join(
            new_word_bank[i:i + max_line_length] for i in range(0, word_bank_length, max_line_length))

        self.word_bank['text'] = new_word_bank
