from enum import Enum


class Question:
    """Holds question data"""

    def __init__(self, name, description, image):
        self.name = name
        self.description = description
        self.image = image


class QuestionResult(Enum):
    """QuestionResult represents the possible values that can exist for the result of a question"""

    CORRECT = 0,
    INCORRECT = 1,
